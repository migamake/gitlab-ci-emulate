{-# LANGUAGE OverloadedStrings #-}

{-|
  Module      : GitLabCi.Shell
  Description : Module that takes information from DockerJobs and Stages 
  and creates shell scripts from them
  Maintainer  : joaopedro.pianta@gmail.com
-}

module GitLabCi.Shell 
  ( Command
  , gitLabCiArtifacts
  , gitLabCiBuild
  , gitLabCiRun
  , gitLabCiScript
  ) where

import qualified Data.Text as T

import GitLabCi.DockerJob (jobNames)
import GitLabCi.Types

-- |Command to build all DockerJobs from a single stage.
buildDockerJobs :: FilePath -> [DockerJob] -> Command
buildDockerJobs path jbs = T.intercalate "\n"
  [ "echo \"Building stage\";"
  , dockerJobArray jbs
  , "for JOB in ${JOBS[@]}; do"
  , T.concat ["  docker build ", T.pack path, ".${JOB} &;"]
  , "done"
  , "wait"
  ]

-- |Command to build all DockerJobs from all stages.
buildStages :: FilePath -> [Stage] -> Command
buildStages path stgs = T.intercalate "\n"
  [ "echo \"Building stages\";"
  , stageJobsArray stgs
  , "for JOB in ${JOBS[@]}; do"
  , T.concat ["  docker build ", T.pack path, ".${JOB} &;"]
  , "done"
  , "wait"
  ]

-- |Creates an array of jobs.
dockerJobArray :: [DockerJob] -> Command
dockerJobArray jbs = foldr1
                      T.append
                      ["JOBS=(", (T.intercalate " " (jobNames jbs)), ")"]

-- |Command to compress all artifacts of a DockerJob into a tar file.
dockerJobArtifacts :: DockerJob -> Command
dockerJobArtifacts DockerJob {name = n, artifacts = Artifact {paths = p}}
  | null p = ""
  | otherwise = T.append
    (T.concat ["tar -czf ./artifacts/", n, "_artifacts.tgz "])
    (T.intercalate " " p)

-- |Creats tar files from all artifacts of a list of DockerJobs.
dockerJobsArtifacts :: [DockerJob] -> Command
dockerJobsArtifacts [] = ""
dockerJobsArtifacts jbs
  | null arts = ""
  | otherwise = T.intercalate "\n"
    [ "# Artifacts"
    , T.intercalate "\n" (map dockerJobArtifacts jbs)
    ]
    where arts = filter (not . T.null) (map dockerJobArtifacts jbs)

-- |Create tar from all artifacts of a GitLabCi file.
gitLabCiArtifacts :: GitLabCi -> Command
gitLabCiArtifacts ci = case ci of
  SingleStage jbs -> dockerJobsArtifacts jbs
  MultiStage stgs -> intercalateCommands (map stageArtifacts stgs)

-- |Build all stages of a GitLabCi file.
gitLabCiBuild :: FilePath -> GitLabCi -> Command
gitLabCiBuild path ci = case ci of
  SingleStage jbs -> buildDockerJobs path jbs
  MultiStage stgs -> buildStages path stgs

-- |Run all stages of a GitLabCi file.
gitLabCiRun :: FilePath -> GitLabCi -> Command
gitLabCiRun path ci = case ci of
  SingleStage jbs -> singleStageRun path jbs
  MultiStage stgs -> intercalateCommands (map (stageRun path) stgs)

-- |Creates a shell script to build and execute a GitLabCi.
gitLabCiScript :: FilePath -> GitLabCi -> Command
gitLabCiScript path ci = intercalateCommands [ gitLabCiArtifacts ci
                                             , gitLabCiBuild path ci
                                             , gitLabCiRun path ci
                                             ]

-- |Intercalate commands, adding a double new line between them and filtering for empty ones.
intercalateCommands :: [Command] -> Command
intercalateCommands = T.intercalate "\n\n" . filter (not . T.null)

-- |Command to run all DockerJobs from a single stage.
runDockerJobs :: FilePath -> [DockerJob] -> Command
runDockerJobs path jbs = T.intercalate "\n"
  [ dockerJobArray jbs
  , "for JOB in ${JOBS[@]}; do"
  , T.concat ["  docker run ", T.pack path, ".${JOB} &;"]
  , "done"
  , "wait"
  ]

-- |Command to run all DockerJobs from a single stage.
singleStageRun :: FilePath -> [DockerJob] -> Command
singleStageRun path jbs = T.intercalate "\n"
  [ "echo \"Starting stage\";"
  , runDockerJobs path jbs
  ]

-- |Creates tar files from all artifacts of a Stage.
stageArtifacts :: Stage -> Command
stageArtifacts Stage {stageName = nm, jobs = jbs}
  | null arts = ""
  | otherwise = T.intercalate "\n"
    [ T.concat ["# Artifacts from ", nm]
    , T.intercalate "\n" arts
    ]
    where arts = filter (not . T.null) (map dockerJobArtifacts jbs)


-- |Creates an array of all the jobs from all stages.
stageJobsArray :: [Stage] -> Command
stageJobsArray stgs = dockerJobArray jobText
  where jb = map (\Stage {jobs = jbs} -> jbs) stgs
        jobText = foldr1 (++) jb

-- |Command to run all stages.
stageRun :: FilePath -> Stage -> Command
stageRun path Stage {stageName = n, jobs = jbs} = T.intercalate "\n"
  [ foldr1 T.append ["echo \"Starting stage ", n, "\";"]
  , runDockerJobs path jbs
  ]
