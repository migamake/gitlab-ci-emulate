{-# LANGUAGE OverloadedStrings #-}

{-|
  Module      : GitlabCi.Variables
  Description : Module that handles parsing and replacement of variables
  Maintainer  : joaopedro.pianta@gmail.com
-}

module GitLabCi.Variables 
  ( replaceVariable
  , replaceVariableBrackets
  , replaceVariables
  , toText
  , unionVars
  ) where

import qualified Data.Text           as T
import qualified Data.HashMap.Strict as HM
import qualified Data.Yaml           as Y

import           Data.Maybe (fromMaybe)
import           Text.Regex.TDFA

import           GitLabCi.VariableStorage

-- |Get name of the variable from inside a brackets format.
getNameFromBrackets :: T.Text -> T.Text
getNameFromBrackets = T.tail . T.tail . T.init

-- |Get name of the variable that comes after a `$`.
getNameFromVariable :: T.Text -> T.Text
getNameFromVariable = T.tail

-- |Get variable value if it is in the storage, otherwise keep the variable
-- name it currently has.
getWordOrKeep :: VariableStorage -> (T.Text -> T.Text) -> T.Text -> T.Text
getWordOrKeep storage getName wordMatch = 
  fromMaybe wordMatch (storage HM.!? getName wordMatch)

-- |Regex that identifies variables on the format `$VAR_NAME`.
regexVariable :: T.Text
regexVariable = T.append "\\$" regexWord

-- |Regex that identifies variables on the format `${VAR_NAME}`.
regexVariableBrackets :: T.Text
regexVariableBrackets = T.concat ["\\$\\{", regexWord, "\\}"]

-- |Regex that identifies a name.
regexWord :: T.Text
regexWord = "([a-zA-Z_][a-zA-Z0-9_]*)"

-- |Replace all occurrences of a variable using a regular expression and a
-- replace function to get its value from the storage.
replace :: T.Text -> (T.Text -> T.Text) -> VariableStorage -> T.Text -> T.Text
replace regexp getName storage text = 
  foldl
    (\current wordMatch -> 
      T.replace
        wordMatch
        (getWordOrKeep storage getName wordMatch)
        current
    )
    text
    matches
      where
        matches :: [T.Text]
        matches = getAllTextMatches $ text =~ regexp

replaceVariable, replaceVariableBrackets :: VariableStorage -> T.Text -> T.Text
replaceVariable = replace regexVariable getNameFromVariable
replaceVariableBrackets = replace regexVariableBrackets getNameFromBrackets

-- |Replace all occurrences of a variable name of all formats to
-- its value from the storage.
replaceVariables :: VariableStorage -> T.Text -> T.Text
replaceVariables storage =  replaceVariable storage . 
                            replaceVariableBrackets storage

-- |Converts a Yaml value into a Text.
toText :: Y.Value -> T.Text
toText val = case val of
  Y.String s -> s
  Y.Number n -> T.pack (show n)
  _ -> error "Variable expected to be a String or a Number"

-- |Takes a Yaml object and merge it with a VariableStorage.
unionVars :: Y.Object -> VariableStorage -> VariableStorage
unionVars objct storage = 
  unionStorages
    (storageMap (replaceVariables storage . toText) objct)
    storage
