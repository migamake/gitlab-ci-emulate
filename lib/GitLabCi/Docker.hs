{-# LANGUAGE OverloadedStrings #-}

{-|
  Module      : GitlabCi.Docker
  Description : Module that creates Dockerfiles based on DockedJob structures
  Maintainer  : joaopedro.pianta@gmail.com
-}

module GitLabCi.Docker 
 ( dockerFileToList
 , dockerJobCommands
 , dockerJobImage
 , dockerJobToDockerFile
 , getDockerFileName
 , gitlabCiDockerFiles
 , gitLabCiToList
 , parseGitLabCi
 , putDockerJobToDockerStr
 , readGitLabCiFile
 , readGitLabCiFileFromStorage
 , writeDockerFileFromGitLabCi
 , writeDockerJobToDockerFile
 , writeGitlabDockerFiles
 , writeDockerJobToDockerFileWithName
 ) where

import qualified Data.Text                   as T
import qualified Data.Text.Lazy              as L
import qualified Data.Yaml                   as Y
import qualified GHC.Base                    as Base
import qualified Language.Docker.PrettyPrint as PrettyPrint
import qualified Language.Docker.Syntax      as Syntax

import           Control.Exception  (throw)
import           Data.Functor
import           Language.Docker.Creator
import           Language.Docker.EDSL
import           System.Environment (getEnvironment)

import           GitLabCi.DockerJob
import           GitLabCi.Types
import           GitLabCi.VariableStorage

-- |Creates a command that copies the DockerJob artifacts.
copyArtifacts :: DockerJob -> EInstructionM ()
copyArtifacts DockerJob {name = n, artifacts = Artifact {paths = p}}
  |null p = return ()
  |otherwise =  copyDirectory
                  (T.concat ["./artifacts/", n, "_artifacts.tgz"])
                  "."
                >>
                run
                  (Syntax.ArgumentsText
                    (T.concat ["tar xzf ./artifacts/", n, "_artifacts.tgz"])
                  )

-- |Creates a command that copies the DockerJob dependencies.
copyDependencies :: DockerJob -> EInstructionM ()
copyDependencies DockerJob {dependencies = deps}
  | null deps = return ()
  | otherwise = sequence_ ((`copyDirectoryFrom` "." ) <$> deps) >>
                sequence_ ((\n -> run
                                    (Syntax.ArgumentsText
                                      (T.concat
                                        [ "tar xzf "
                                        , n
                                        , "_artifacts.tgz"
                                        ]
                                      )
                                    )
                           )
                          <$>
                          deps
                          )

-- |Copy a directory to a target
copyDirectory :: T.Text -> T.Text -> EInstructionM ()
copyDirectory source dest = copy
                              (Syntax.CopyArgs
                                (Syntax.SourcePath
                                source
                                Base.:|
                                []
                                )
                              (Syntax.TargetPath dest)
                              Syntax.NoChown
                              Syntax.NoSource
                              )

-- |Creates a command that copies a directory from another stage.
copyDirectoryFrom :: T.Text -> T.Text -> EInstructionM ()
copyDirectoryFrom n dest = copy $
                          (Syntax.SourcePath
                            (T.append n "_artifacts.tgz")
                            Base.:|
                            []
                          ) `to`
                          Syntax.TargetPath
                            dest
                            `fromStage`
                            Syntax.CopySource n

-- |Creates a list of commands as Texts from a DockerJob.
-- This is mainly used for testing.
dockerFileToList :: DockerJob -> [L.Text]
dockerFileToList =  filter (/= "") .
                    L.splitOn "\n" .
                    PrettyPrint.prettyPrint .
                    toDockerfile .
                    dockerJobToDockerFile

-- |Converts a list of Texts into a sequence of CMD Dockerfile instructions.
-- We convert to CMD commands instead of RUN because we cannot know from the
-- Yaml description the type of the desired persistence.
-- This is used for the commands field of the DockerJob.
dockerJobCommands :: [T.Text] -> EInstructionM ()
dockerJobCommands = foldr ((>>) . cmd . Syntax.ArgumentsText) (return ())

-- |Converts a Text into a FROM Dockerfile instruction.
-- This is used for the image field of the DockerJob.
dockerJobImage :: T.Text -> T.Text -> EInstructionM ()
dockerJobImage n = from . getImage n

-- |Extract all stages from a list of DockerJobs.
dockerJobsFromStage :: T.Text -> [DockerJob] -> Stage
dockerJobsFromStage stgName jbs = Stage
                                    stgName
                                    (filter
                                      (\DockerJob {stage = s} -> s == stgName)
                                      jbs
                                    )

-- |Creates a series of Dockerfile instructions based on a DockerJob.
dockerJobToDockerFile :: DockerJob -> EInstructionM ()
dockerJobToDockerFile dj@DockerJob {name = n, image = i, commands = cmds} = 
  dockerJobImage n i >>
  copyArtifacts dj >>
  copyDependencies dj >>
  dockerJobCommands cmds

-- |Auxiliary function to get a Dockerfile name for a given DockerJob.
getDockerFileName :: FilePath -> T.Text -> T.Text
getDockerFileName path nm = foldr T.append "" [T.pack path, ".", nm]

-- |Helper function to create an EBaseImage from a Text.
-- An image has the formate image:tag, so we split the image name at : to get
-- name and tag (if it has one).
-- This will create an image in the format `FROM image:tag AS alias`
getImage :: T.Text -- ^ The name of the stage to be used as the alias
         -> T.Text -- ^ The image text with image and tag
         -> EBaseImage
getImage nm img = getImageFromSplittedText splittedImg nm
  where 
    splittedImg = T.splitOn ":" img
    getImageFromSplittedText :: [T.Text] -> T.Text -> EBaseImage
    getImageFromSplittedText [] _               = 
      error "Did not excpect empty string"
    getImageFromSplittedText (imag : tag : _) n = 
      EBaseImage
        (Syntax.Image Nothing imag)
        (Just (Syntax.Tag tag))
        Nothing
        (Just (Syntax.ImageAlias n))
        Nothing
    getImageFromSplittedText (imag : _) n       = 
      EBaseImage
        (Syntax.Image Nothing imag)
        Nothing
        Nothing
        (Just (Syntax.ImageAlias n))
        Nothing

-- |Creates a series of Dockerfile instructions based on all the DockerJobs
-- of a GitLab CI Yaml file.
gitlabCiDockerFiles :: [DockerJob] -> [EInstructionM ()]
gitlabCiDockerFiles = map dockerJobToDockerFile

-- |Creates a list of commands as Texts from a DockerJob.
-- This is mainly used for testing.
gitLabCiToList :: GitLabCi -> [[L.Text]]
gitLabCiToList ci = case ci of
  SingleStage jbs -> dockerFilesToList jbs
  MultiStage stgs -> concatMap (\ Stage{jobs=j} -> dockerFilesToList j) stgs
  where
    -- |Creates a list of commands as Texts from a DockerJob.
    -- This is mainly used for testing.
    dockerFilesToList :: [DockerJob] -> [[L.Text]]
    dockerFilesToList = map dockerFileToList

-- |Create a GitLabCi object from a GitLabCi Yaml file.
parseGitLabCi :: Y.Object -> VariableStorage -> GitLabCi
parseGitLabCi obj storage
  | null stgs = SingleStage jbs
  | otherwise = MultiStage (map (`dockerJobsFromStage` jbs) stgs)
  where 
    stgs = getStageNames obj
    jbs = parseDockerJobs obj storage

-- |Prints the Dockerfile instructions.
putDockerJobToDockerStr :: DockerJob -> IO ()
putDockerJobToDockerStr = putDockerfileStr . dockerJobToDockerFile

-- |Read and parse a Yaml file and return all of its DockerJobs.
readGitLabCiFile :: FilePath -> IO GitLabCi
readGitLabCiFile path = do
  res <- Y.decodeFileEither path :: IO (Either Y.ParseException Y.Object)
  case res of
    Right obj -> getEnvironment <&> (parseGitLabCi obj . storageFromList)
    Left err  -> throw err

-- |Read and parse a Yaml file and return all of its DockerJobs, 
-- using a VariableStorage as a base. This is mainly used for testing purposes,
-- so that we do not use the environment variables from the testing system.
readGitLabCiFileFromStorage :: FilePath -> VariableStorage -> IO GitLabCi
readGitLabCiFileFromStorage path storage = do
  res <- Y.decodeFileEither path :: IO (Either Y.ParseException Y.Object)
  case res of
    Right obj -> return (parseGitLabCi obj storage)
    Left err  -> throw err

-- |Creates multiple Dockerfiles from the jobs of a Stage.
writeCiStages :: FilePath -> Stage -> IO ()
writeCiStages path (Stage {jobs = j}) = sequence_ 
                                          (writeDockerJobToDockerFileWithName
                                            path <$> j
                                          )

-- |Given a GitLab CI Yaml file file path and an output file path,
-- creates multiple Dockerfiles on that location.
writeDockerFileFromGitLabCi :: FilePath -> FilePath -> IO ()
writeDockerFileFromGitLabCi inp out = readGitLabCiFile inp >>= 
                                      writeGitlabDockerFiles out

-- |Prints the Dockerfile instructions of a DockerJob into a file.
writeDockerJobToDockerFile :: T.Text -> DockerJob -> IO ()
writeDockerJobToDockerFile path = writeDockerFile path .
                                  (toDockerfile . dockerJobToDockerFile)

-- |Creates multiple Dockerfiles from the jobs of a GitLab CI Yaml file
-- using a base path. For example, if a a path /"example\/Dockerfile/" is given
-- and a /"build/" job is in the Yaml, a /"example\/Dockerfile.build/"
-- will be created.
writeGitlabDockerFiles :: FilePath -> GitLabCi -> IO ()
writeGitlabDockerFiles path ci = case ci of
  SingleStage jbs -> sequence_ 
                      (writeDockerJobToDockerFileWithName path <$> jbs)
  MultiStage stgs -> sequence_ 
                      (writeCiStages path <$> stgs)

-- |Prints the Dockerfile instructions of a DockerJob into a file using the
-- DockerJob name for the file path.
writeDockerJobToDockerFileWithName :: FilePath -> DockerJob -> IO ()
writeDockerJobToDockerFileWithName path dj@DockerJob {name=n} = 
  writeDockerFile
    (getDockerFileName path n)
    (toDockerfile (dockerJobToDockerFile dj))
