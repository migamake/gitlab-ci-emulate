{-|
  Module      : GitlabCi.Types
  Description : Module that defines type constructors, initializers and
  type-specific functions
  Maintainer  : joaopedro.pianta@gmail.com
-}

module GitLabCi.Types 
  ( Artifact (..)
  , Command
  , DockerJob (..)
  , GitLabCi (..)
  , Stage (..)
  , emptyArtifact
  , emptyDockerJob
  ) where

import qualified Data.HashMap.Strict as HM
import qualified Data.Text           as T

import GitLabCi.VariableStorage

data Artifact = Artifact { paths :: [T.Text] } deriving (Show, Eq)

-- |Alias to help identify commands.
type Command = T.Text

-- |Describes what a Docker Job is.
data DockerJob = DockerJob 
  { name :: T.Text
  , image :: T.Text
  , commands :: [T.Text]
  , variables :: VariableStorage
  , stage :: T.Text
  , dependencies :: [T.Text]
  , artifacts :: Artifact
  } deriving (Show, Eq)

-- |Describes multi stage or single stage Cis.
data GitLabCi = MultiStage [Stage] | SingleStage [DockerJob]
  deriving (Show, Eq)

-- |Describes properties of a Stage.
data Stage = Stage { stageName :: T.Text, jobs :: [DockerJob] }
  deriving (Show, Eq)

-- |Definition of an empty Artifact.
emptyArtifact :: Artifact
emptyArtifact = Artifact []

-- |Definition of an empty DockerJob.
emptyDockerJob :: DockerJob
emptyDockerJob = DockerJob T.empty T.empty [] HM.empty T.empty [] emptyArtifact
