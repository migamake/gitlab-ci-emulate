{-|
  Module      : GitlabCi.VariableStorage
  Description : Module that defines type constructors, initializers and
  type-specific functions for the VariableStorage data type
  Maintainer  : joaopedro.pianta@gmail.com
-}

module GitLabCi.VariableStorage 
  ( VariableStorage
  , emptyVariableStorage
  , mergeFromList
  , storageFromList
  , storageInsert
  , storageMap
  , unionStorages
  ) where

import qualified Data.HashMap.Strict as HM
import qualified Data.Text           as T

type VariableStorage = HM.HashMap T.Text T.Text

-- |Definition of an empty VariableStorage.
emptyVariableStorage :: VariableStorage
emptyVariableStorage = HM.empty

mergeFromList :: [(String, String)] -> VariableStorage -> VariableStorage
mergeFromList list = unionStorages (storageFromList list)

-- |Creates a VariableStorage from a list of string pairs.
-- Used for creating a VariableStorage from a list of environment variables. 
storageFromList :: [(String, String)] -> VariableStorage
storageFromList = HM.fromList . map toTextPair

storageInsert :: T.Text -> T.Text -> VariableStorage -> VariableStorage
storageInsert = HM.insert

storageMap :: (t -> T.Text) -> HM.HashMap T.Text t -> VariableStorage
storageMap = HM.map

-- |Converts string pairs into text pairs.
toTextPair :: (String, String) -> (T.Text, T.Text)
toTextPair (s1, s2) = (T.pack s1, T.pack s2)

-- |Alias to merge VariableStorages.
unionStorages :: VariableStorage -> VariableStorage -> VariableStorage
unionStorages = HM.union
