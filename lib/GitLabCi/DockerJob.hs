{-# LANGUAGE OverloadedStrings #-}

{-|
  Module      : GitlabCi.DockerJob
  Description : Module that parses Yaml files and creates DockerJob structures
  Maintainer  : joaopedro.pianta@gmail.com

  DockerJob is used to extract information regarding the docker jobs from the GitLab CI Yaml files.
-}

module GitLabCi.DockerJob 
  ( getStageNames
  , jobNames
  , parseDockerJobs
  , updateDockerJobArtifacts
  , updateDockerJobCommands
  , updateDockerJobDependencies
  , updateDockerJobImage
  , updateDockerJobVariables
  ) where

import qualified Data.HashMap.Strict   as HM
import qualified Data.Text             as T
import qualified Data.Vector           as V
import qualified Data.Yaml             as Y

import           GitLabCi.Types
import           GitLabCi.Variables
import           GitLabCi.VariableStorage

-- |Since Yaml Array is a recursive defition of a Vector of values, we have
--  this function to get all the elements that are of type Text
--  and place them of a list.
arrayToStringVec :: Y.Array -> [T.Text]
arrayToStringVec a
  | V.null a = []
  | otherwise =
    case V.head a of
      Y.String v -> v : arrayToStringVec (V.tail a)
      _          -> arrayToStringVec (V.tail a)

-- |Get the name of all stages of a Ci file.
getStageNames :: Y.Object -> [T.Text]
getStageNames obj =
  case obj HM.!? "stages" of
    Just (Y.Array a) -> arrayToStringVec a
    Nothing          -> []
    Just _           -> error "Expected stages to be a list of commands"

-- |Verify if word is a keyword.
hasKey :: T.Text -> [T.Text] -> Bool
hasKey _ [] = False
hasKey k (hd : tl)
  | k == hd = True
  | otherwise = hasKey k tl

-- |Get all names of a list of DockerJobs.
jobNames :: [DockerJob] -> [T.Text]
jobNames = map name

-- |List of reserved keywords that are definitions, not job names.
keywords :: [T.Text]
keywords = ["scripts", "variables"]

-- |Creates an Artifact based on an Object and the current scope of an 
-- Artifact. This currently is just an alias, but since Artifact will have more
-- attributes in the future, it will keep a pattern with `parseJob`
--  and parse all attributes
parseArtifact :: Y.Object -> Artifact -> VariableStorage -> Artifact
parseArtifact = updateArtifactPaths

-- |Take all the pairs key -> object, where key is not a keyword, 
-- and create a DockerJob based on a current DockerJob.
_parseDockerJobs :: DockerJob -> [(T.Text, Y.Value)] -> [DockerJob]
_parseDockerJobs _ [] = []
_parseDockerJobs dj ((key, val) : tl)
  | not (hasKey key keywords) =
    case val of
      Y.Object obj -> parseJob obj dj {name = key} : _parseDockerJobs dj tl
      _ -> _parseDockerJobs dj tl
  | otherwise = _parseDockerJobs dj tl

-- |Creates the base DockerJob from the global scope and return all the
--  DockerJobs of the file.
parseDockerJobs :: Y.Object -> VariableStorage -> [DockerJob]
parseDockerJobs obj storage = _parseDockerJobs 
                                (parseJob
                                  obj
                                  (emptyDockerJob {variables = storage})
                                )
                                (HM.toList obj)

-- |Creates a DockerJob based on an Object and the current scope of a
-- DockerJob.
parseJob :: Y.Object -> DockerJob -> DockerJob
parseJob obj job =
  updateDockerJobImage obj $
    updateDockerJobStage obj $
      updateDockerJobCommands obj $
        updateDockerJobDependencies obj $
          updateDockerJobArtifacts obj $
            updateDockerJobVariables obj job

-- We have an ideia of scopes in the Yaml file, 
-- so these update functions take a current DockerJob and a Yaml object and
-- merge them to get the DockerJob of an inner scope.

-- Artifact update functions begin

-- |Replace the paths definition of an Artifact.
updateArtifactPaths :: Y.Object -> Artifact -> VariableStorage -> Artifact
updateArtifactPaths obj art storage =
  case obj HM.!? "paths" of
    Just (Y.Array val) -> art {paths = 
                                      map 
                                        (replaceVariables storage)
                                        (arrayToStringVec val)
                              }
    Nothing            -> art
    Just _             -> error "Expected paths to be a list"

-- Artifact update functions end

-- Docker Job update functions begin

-- |Replace the image definition of a DockerJob.
updateDockerJobArtifacts :: Y.Object -> DockerJob -> DockerJob
updateDockerJobArtifacts obj dj@DockerJob { artifacts = arts
                                          , variables = storage
                                          } =
  case obj HM.!? "artifacts" of
    Just (Y.Object o) -> dj {artifacts = (parseArtifact o arts storage)}
    Nothing           -> dj
    Just _            -> error "Expected artifacts to be a map"

-- |Replace the script definition of a DockerJob.
updateDockerJobCommands :: Y.Object -> DockerJob -> DockerJob
updateDockerJobCommands obj dj@DockerJob{variables = storage} =
  case obj HM.!? "script" of
    Just (Y.Array val)  -> dj {commands = 
                                          map
                                            (replaceVariables storage)
                                            (arrayToStringVec val)
                              }
    Just (Y.String val) -> dj {commands = [replaceVariables storage val]}
    Nothing             -> dj
    Just _              -> error "Expected script to be a list of commands or a string with a single command"

-- |Replace the script definition of a DockerJob.
updateDockerJobDependencies :: Y.Object -> DockerJob -> DockerJob
updateDockerJobDependencies obj dj@DockerJob{variables = storage} =
  case obj HM.!? "dependencies" of
    Just (Y.Array val) -> dj {dependencies = 
                                            map
                                              (replaceVariables storage)
                                              (arrayToStringVec val)
                              }
    Nothing            -> dj
    Just _             -> error "Expected dependencies to be a list"

-- |Replace the image definition of a DockerJob.
updateDockerJobImage :: Y.Object -> DockerJob -> DockerJob
updateDockerJobImage obj dj@DockerJob{variables = storage} =
  case obj HM.!? "image" of
    Just (Y.String val) -> dj {image = replaceVariables storage val}
    Nothing             -> dj
    Just _              -> error "Expected image to be a string"

-- |Replace the image definition of a DockerJob.
updateDockerJobStage :: Y.Object -> DockerJob -> DockerJob
updateDockerJobStage obj dj@DockerJob{variables = storage} =
  case obj HM.!? "stage" of
    Just (Y.String val) -> dj {stage = replaceVariables storage val}
    Nothing             -> dj
    Just _              -> error "Expected stage to be a string"

-- |Merge the variable definitions of a DockerJob.
updateDockerJobVariables :: Y.Object -> DockerJob -> DockerJob
updateDockerJobVariables obj dj@DockerJob {variables = vars} =
  case obj HM.!? "variables" of
    Just (Y.Object o) -> dj {variables = unionVars o vars}
    Nothing           -> dj
    Just _            -> error "Expected variables to be a map"

-- Docker Job update functions end
