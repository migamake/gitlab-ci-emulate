{-# LANGUAGE OverloadedStrings #-}

module ShellSpec (spec) where

import qualified Data.Text as T
import           GitLabCi.Docker
import           Test.Hspec

import           GitLabCi.Shell

spec :: Spec
spec = do
  describe "gitLabCiRun" $ do
    describe "simple" $ do
      it "create a shell script to run all stages" $
        readGitLabCiFile "examples/simple/.gitlab-ci.yml" >>=
        shouldBe
          (T.intercalate "\n"
            [ "echo \"Starting stage\";"
            , "JOBS=(build)"
            , "for JOB in ${JOBS[@]}; do"
            , "  docker run Dockerfile.${JOB} &;"
            , "done"
            , "wait"
            ]
          )
          .
          gitLabCiRun "Dockerfile" 

    describe "dind-use" $ do
      it "create a shell script to run all stages" $
        readGitLabCiFile "examples/dind-use-image/.gitlab-ci.yml" >>=
        shouldBe
          (T.intercalate "\n"
            [ "echo \"Starting stage build\";"
            , "JOBS=(scrape)"
            , "for JOB in ${JOBS[@]}; do"
            , "  docker run Dockerfile.${JOB} &;"
            , "done"
            , "wait"
            , ""
            , "echo \"Starting stage validate\";"
            , "JOBS=(parse)"
            , "for JOB in ${JOBS[@]}; do"
            , "  docker run Dockerfile.${JOB} &;"
            , "done"
            , "wait"
            ]
          )
          .
          gitLabCiRun "Dockerfile"

    describe "dind-build" $ do
      it "create a shell script to run all stages" $
        readGitLabCiFile
          "examples/dind-build-image/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "echo \"Starting stage build\";"
              , "JOBS=(stack_build)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              , ""
              , "echo \"Starting stage docker\";"
              , "JOBS=(docker_image)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              , ""
              , "echo \"Starting stage deploy\";"
              , "JOBS=(push_container)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              ]
            )
            .
            gitLabCiRun "Dockerfile"

  describe "gitLabCiBuild" $ do
    describe "simple" $ do
      it "create a shell script to build all stages" $
        readGitLabCiFile
          "examples/simple/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "echo \"Building stage\";"
              , "JOBS=(build)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker build Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              ]
            )
            .
            gitLabCiBuild "Dockerfile"

    describe "dind-use" $ do
      it "create a shell script to build all stages" $
        readGitLabCiFile
          "examples/dind-use-image/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "echo \"Building stages\";"
              , "JOBS=(scrape parse)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker build Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              ]
            )
            .
            gitLabCiBuild "Dockerfile"
        

    describe "dind-build" $ do
      it "create a shell script to build all stages" $
        readGitLabCiFile
          "examples/dind-build-image/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "echo \"Building stages\";"
              , "JOBS=(stack_build docker_image push_container)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker build Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              ]
            )
            .
            gitLabCiBuild "Dockerfile" 

  describe "gitLabCiArtifacts" $ do
    describe "simple" $ do
      it "create a shell script to compress all artifacts from all stages" $
        readGitLabCiFile
          "examples/simple/.gitlab-ci.yml"
        >>=
          shouldBe
            ""
            .
            gitLabCiArtifacts

    describe "dind-use" $ do
      it "create a shell script to compress all artifacts from all stages" $
        readGitLabCiFile
          "examples/dind-use-image/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "# Artifacts from build"
              , "tar -czf ./artifacts/scrape_artifacts.tgz output/*.csv"
              ]
            )
            .
            gitLabCiArtifacts

    describe "dind-build" $ do
      it "create a shell script to compress all artifacts from all stages" $
        readGitLabCiFile
          "examples/dind-build-image/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "# Artifacts from build"
              , "tar -czf ./artifacts/stack_build_artifacts.tgz sdist/$PACKAGE_NAME-*[0-9].tar.gz hackage-docs/$PACKAGE_NAME-*-docs.tar.gz"
              , ""
              , "# Artifacts from docker"
              , "tar -czf ./artifacts/docker_image_artifacts.tgz curl-parse-exe.docker"
              ]
            )
            .
            gitLabCiArtifacts

  describe "gitLabCiScript" $ do
    describe "simple" $ do
      it "create a shell script to build and run all stages" $
        readGitLabCiFile
          "examples/simple/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "echo \"Building stage\";"
              , "JOBS=(build)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker build Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              , ""
              , "echo \"Starting stage\";"
              , "JOBS=(build)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              ]
            )
            .
            gitLabCiScript "Dockerfile"

    describe "dind-use" $ do
      it "create a shell script to build and run all stages" $
        readGitLabCiFile
          "examples/dind-use-image/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "# Artifacts from build"
              , "tar -czf ./artifacts/scrape_artifacts.tgz output/*.csv"
              , ""
              , "echo \"Building stages\";"
              , "JOBS=(scrape parse)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker build Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              , ""
              , "echo \"Starting stage build\";"
              , "JOBS=(scrape)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              , ""
              , "echo \"Starting stage validate\";"
              , "JOBS=(parse)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              ]
            )
            .
            gitLabCiScript "Dockerfile"

    describe "dind-build" $ do
      it "create a shell script to build and run all stages" $
        readGitLabCiFile
          "examples/dind-build-image/.gitlab-ci.yml"
        >>=
          shouldBe
            (T.intercalate "\n"
              [ "# Artifacts from build"
              , "tar -czf ./artifacts/stack_build_artifacts.tgz sdist/$PACKAGE_NAME-*[0-9].tar.gz hackage-docs/$PACKAGE_NAME-*-docs.tar.gz"
              , ""
              , "# Artifacts from docker"
              , "tar -czf ./artifacts/docker_image_artifacts.tgz curl-parse-exe.docker"
              , ""
              , "echo \"Building stages\";"
              , "JOBS=(stack_build docker_image push_container)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker build Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              , ""
              , "echo \"Starting stage build\";"
              , "JOBS=(stack_build)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              , ""
              , "echo \"Starting stage docker\";"
              , "JOBS=(docker_image)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              , ""
              , "echo \"Starting stage deploy\";"
              , "JOBS=(push_container)"
              , "for JOB in ${JOBS[@]}; do"
              , "  docker run Dockerfile.${JOB} &;"
              , "done"
              , "wait"
              ]
            )
            .
            gitLabCiScript "Dockerfile"
