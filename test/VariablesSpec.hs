{-# LANGUAGE OverloadedStrings #-}

module VariablesSpec (spec) where

import           Test.Hspec

import           GitLabCi.Variables
import           GitLabCi.VariableStorage

spec :: Spec
spec = do
  describe "test" $ do
    describe "replaceVariableBrackets" $ do
      it "replace variables inside brackets" $
        shouldBe
          (replaceVariableBrackets
            (storageFromList [("TEST_A", "A"), ("TEST_B", "B")])
            "ab ${TEST_A} ${TEST_B}"
          )
          "ab A B"

      it "replace variables without brackets, but keep variable names that are not in the storage" $
        shouldBe
          (replaceVariableBrackets
            (storageFromList [("TEST_A", "A"), ("TEST_B", "B")])
            "ab ${TEST_A} ${TEST_B} ${TEST_C}"
          )
          "ab A B ${TEST_C}"

    describe "replaceVariable" $ do
      it "replace variables without brackets" $
        shouldBe
          (replaceVariable
            (storageFromList [("TEST_A", "A"), ("TEST_B", "B")])
            "ab $TEST_A $TEST_B"
          )
          "ab A B"

      it "replace variables without brackets, but keep variable names that are not in the storage" $
        shouldBe
          (replaceVariable
            (storageFromList [("TEST_A", "A"), ("TEST_B", "B")])
            "ab $TEST_A $TEST_B $TEST_C"
          )
          "ab A B $TEST_C"

    describe "replaceVariables" $ do
      it "replace variables with both types of substitutions" $
        shouldBe
          (replaceVariables
            (storageFromList [("TEST_A", "A"), ("TEST_B", "B")])
            "ab $TEST_A ${TEST_B}"
          )
          "ab A B"

      it "replace variables with both types of substitutions, but keep variable names that are not in the storage" $
        shouldBe
          (replaceVariables
            (storageFromList [("TEST_A", "A"), ("TEST_B", "B")])
            "ab $TEST_A ${TEST_B} ${TEST_C} $TEST_D"
          )
          "ab A B ${TEST_C} $TEST_D"
