{-# LANGUAGE OverloadedStrings #-}

module DockerJobSpec (spec) where

import           GitLabCi.Docker
import           GitLabCi.DockerJob
import           GitLabCi.Types
import           GitLabCi.VariableStorage
import           Test.Hspec
import           Utils

spec :: Spec
spec = do
  describe "updateDockerJobImage" $ do
    it "should set image if there is none" $
      shouldBe
        (updateDockerJobImage yaml1 emptyDockerJob)
        (emptyDockerJob {image = "migamake/stack-build-image:16.28"})

    it "should overwrite image if there is one" $
      shouldBe
        (updateDockerJobImage yaml1 emptyDockerJob {image = "ubuntu"})
        (emptyDockerJob {image = "migamake/stack-build-image:16.28"})

  describe "updateDockerJobCommands" $ do
    it "should set commands if there is none" $
      shouldBe
        (updateDockerJobCommands yaml2 emptyDockerJob)
        (emptyDockerJob {commands = ["./test2.sh", "./test.sh"]})

    it "should overwrite commands if there is one" $
      shouldBe
        (updateDockerJobCommands
          yaml2
          emptyDockerJob
          {commands = ["./test0.sh"]}
        )
        (emptyDockerJob {commands = ["./test2.sh", "./test.sh"]})

  describe "updateDockerJobVariables" $ do
    it "should set variables if there is none" $
      shouldBe
        (updateDockerJobVariables yaml3 emptyDockerJob)
        (emptyDockerJob {variables = 
                                    storageFromList [ ("x", "test")
                                                    , ("y", "test2")
                                                    ]
                        }
        )

    it "should merge variables if there is one" $
      shouldBe
        (updateDockerJobVariables
          yaml3
          emptyDockerJob  {variables  =
                                      storageFromList [ ("x", "bla")
                                                      , ("z", "test2")
                                                      ]
                          }
        )
        (emptyDockerJob  {variables = 
                                    storageFromList [ ("x", "test")
                                                    , ("y", "test2")
                                                    , ("z", "test2")
                                                    ]
                        }
        )

  describe "readGitLabCiFile" $ do
    describe "simple" $
      it "should create DockerJobs for each job defined" $
        readGitLabCiFileFromStorage
          "examples/simple/.gitlab-ci.yml"
          emptyVariableStorage 
        >>=
          shouldBe
            (SingleStage  [emptyDockerJob 
                            { name = "build"
                            , image = "migamake/stack-build-image:16.28"
                            , commands = ["./test.sh"]
                            }
                          ]
            )
    
    describe "dind-use-image" $ do
      it "should create DockerJobs for each job defined" $
        let vars = storageFromList
                        [ ("GIT_SUBMODULE_STRATEGY", "recursive")
                        , ("LC_ALL", "C.UTF-8")
                        , ("DOCKER_HOST", "tcp://docker:2376")
                        , ("DOCKER_TLS_CERTDIR", "/certs")]
        in
          readGitLabCiFileFromStorage
            "examples/dind-use-image/.gitlab-ci.yml"
            emptyVariableStorage
          >>=
            shouldBe
              (MultiStage
                [Stage "build"
                  [
                    emptyDockerJob 
                      { name = "scrape"
                      , image = "migamake/chromedriver:28-april-2020"
                      , commands = ["./ci/run.sh"]
                      , variables = vars
                      , stage = "build"
                      , artifacts = (emptyArtifact {paths=["output/*.csv"]})
                      }
                  ]
                ,Stage "validate"
                  [
                    emptyDockerJob 
                      { name = "parse"
                      , image = "docker:19.03.1"
                      , commands = ["ls -alth ./output", "./ci/validate.sh"]
                      , variables = vars
                      , stage = "validate"
                      , dependencies = ["scrape"]
                      }
                  ]
                ]
              )

    describe "dind-build-image" $ do
      it "should create DockerJobs for each job defined" $
        let 
          vars = appendIntPair
                    (storageFromList  [ ("DOCKER_TLS_CERTDIR", "/certs")
                                      , ("GIT_SUBMODULE_STRATEGY", "recursive")
                                      , ("IMAGE_NAME", "curl-parse-exe")
                                      ]
                    )
                    ("GIT_DEPTH", 5)
          env = storageFromList [ ("PACKAGE_NAME", "test_package")
                                , ("CI_REGISTRY", "registry")
                                , ("CI_REGISTRY_USER", "user")
                                , ("CI_REGISTRY_PASSWORD", "pass")
                                , ("CI_PROJECT_PATH", "path/project")
                                , ("CI_COMMIT_REF_SLUG", "slug123")
                                  ]
        in
          readGitLabCiFileFromStorage
            "examples/dind-build-image/.gitlab-ci.yml"
            env
          >>=
            shouldBe
              (MultiStage
                [Stage "build"
                  [
                    emptyDockerJob 
                      { name = "stack_build"
                      , image = "migamake/stack-build-image:14.27"
                      , commands = ["ci/stack-build.sh"]
                      , variables = unionStorages vars env
                      , stage = "build"
                      , artifacts = 
                        (emptyArtifact
                          {paths = [ "sdist/test_package-*[0-9].tar.gz"
                                    , "hackage-docs/test_package-*-docs.tar.gz"
                                    ]
                          }
                        )
                      }
                  ]
                ,Stage "docker"
                  [
                    emptyDockerJob
                      { name = "docker_image"
                      , image = "docker"
                      , commands =
                          [ "docker build . -f Dockerfile -t curl-parse-exe"
                          , "docker save curl-parse-exe -o curl-parse-exe.docker"
                          ]
                      , variables = unionStorages vars env
                      , stage = "docker"
                      , artifacts = (emptyArtifact
                                      {paths=["curl-parse-exe.docker"]}
                                    )
                      }
                  ]
                ,Stage "deploy"
                  [
                    emptyDockerJob 
                      { name = "push_container"
                      , image = "docker"
                      , commands = 
                          [ "docker login -u user -p pass registry"
                          , "docker load -i curl-parse-exe.docker"
                          , "echo docker tag curl-parse-exe -t registry/path/project:slug123"
                          , "docker tag \"curl-parse-exe\" \"registry/path/project:slug123\""
                          , "docker push registry/path/project:slug123;"
                          ]
                      , variables =
                          (mergeFromList
                            [ ("TARGET_TAG", "registry/path/project:slug123")
                            , ("LATEST_TAG", "registry/path/project:latest")
                            ]
                            (unionStorages vars env)
                          )
                      , stage = "deploy"
                      , dependencies = ["docker_image"]
                      }
                  ]
                ]
              )
