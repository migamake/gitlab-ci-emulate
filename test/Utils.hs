module Utils 
 ( _decode
 , appendIntPair
 , toLazyList
 , shouldMatchLazyList
 , yaml1
 , yaml2
 , yaml3
 ) where

import qualified Data.ByteString.Char8 as BS
import qualified Data.Text             as T
import qualified Data.Text.Lazy        as L
import qualified Data.Yaml             as Y

import           Control.Exception     (throw)
import           Data.Scientific       (Scientific)
import           Test.Hspec

import           GitLabCi.VariableStorage

_decode :: String -> Y.Object
_decode s =
  case Y.decodeEither' (BS.pack s) :: Either Y.ParseException Y.Object of
    Right obj -> obj
    Left err  -> throw err

shouldMatchLazyList :: [[String]] -> [[L.Text]] -> Expectation
shouldMatchLazyList x y = map toLazyList x `shouldMatchList` y

appendIntPair :: VariableStorage -> (T.Text, Scientific) -> VariableStorage
appendIntPair storage (a, b) = storageInsert a (T.pack (show b)) storage

toLazyList :: [String] -> [L.Text]
toLazyList = map L.pack

yaml1, yaml2, yaml3 :: Y.Object
yaml1 = _decode "image: migamake/stack-build-image:16.28\n\nbuild:\n tags:\n  - docker\n script:\n  - ./test.sh"

yaml2 = _decode "image: migamake/stack-build-image:16.28\nscript:\n - ./test2.sh\n - ./test.sh"

yaml3 = _decode "image: migamake/stack-build-image:16.28\nvariables:\n x: \"test\"\n y: \"test2\""
