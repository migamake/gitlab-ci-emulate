{-# LANGUAGE OverloadedStrings #-}

module DockerSpec (spec) where

import           GitLabCi.Docker
import           GitLabCi.VariableStorage
import           Test.Hspec
import           Utils

spec :: Spec
spec = do
  describe "GitLab CI to Dockerfile" $ do
    describe "simple" $ do
      it "should create the correct dockerfiles for the simple test GitLab CI" $
        readGitLabCiFileFromStorage
          "examples/simple/.gitlab-ci.yml"
          emptyVariableStorage
        >>=
          shouldMatchLazyList
            [
              [ "FROM migamake/stack-build-image:16.28 AS build"
              , "CMD ./test.sh"
              ]
            ]
            .
            gitLabCiToList

    describe "dind-use-image" $ do
      it "should create the correct dockerfiles for the dind-use test GitLab CI" $
        readGitLabCiFileFromStorage
          "examples/dind-use-image/.gitlab-ci.yml"
          emptyVariableStorage
        >>=
          shouldMatchLazyList
            [
              [ "FROM migamake/chromedriver:28-april-2020 AS scrape"
              , "COPY ./artifacts/scrape_artifacts.tgz ."
              , "RUN tar xzf ./artifacts/scrape_artifacts.tgz"
              , "CMD ./ci/run.sh"
              ]
            ,[ "FROM docker:19.03.1 AS parse"
              , "COPY --from=scrape scrape_artifacts.tgz ."
              , "RUN tar xzf scrape_artifacts.tgz"
              , "CMD ls -alth ./output"
              , "CMD ./ci/validate.sh"
              ]
            ]
            .
            gitLabCiToList

    describe "dind-build-image" $ do
      it "should create the correct dockerfiles for the dind-build test GitLab CI" $
        let env = storageFromList
                    [ ("PACKAGE_NAME", "test_package")
                    , ("CI_REGISTRY", "registry")
                    , ("CI_REGISTRY_USER", "user")
                    , ("CI_REGISTRY_PASSWORD", "pass")
                    , ("CI_PROJECT_PATH", "path/project")
                    , ("CI_COMMIT_REF_SLUG", "slug123")
                    ]
        in
          readGitLabCiFileFromStorage
            "examples/dind-build-image/.gitlab-ci.yml"
            env
          >>=
            shouldMatchLazyList
              [
                [ "FROM migamake/stack-build-image:14.27 AS stack_build"
                , "COPY ./artifacts/stack_build_artifacts.tgz ."
                , "RUN tar xzf ./artifacts/stack_build_artifacts.tgz"
                , "CMD ci/stack-build.sh"
                ],
                [ "FROM docker AS docker_image"
                , "COPY ./artifacts/docker_image_artifacts.tgz ."
                , "RUN tar xzf ./artifacts/docker_image_artifacts.tgz"
                , "CMD docker build . -f Dockerfile -t curl-parse-exe"
                , "CMD docker save curl-parse-exe -o curl-parse-exe.docker"
                ],
                [ "FROM docker AS push_container"
                , "COPY --from=docker_image docker_image_artifacts.tgz ."
                , "RUN tar xzf docker_image_artifacts.tgz"
                , "CMD docker login -u user -p pass registry"
                , "CMD docker load -i curl-parse-exe.docker"
                , "CMD echo docker tag curl-parse-exe -t registry/path/project:slug123"
                , "CMD docker tag \"curl-parse-exe\" \"registry/path/project:slug123\""
                , "CMD docker push registry/path/project:slug123;"
                ]
              ]
              .
              gitLabCiToList
