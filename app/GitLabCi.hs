{-|
  Module      : Main
  Description : Module that parses Yaml files and creates Dockerfiles from the described jobs
  Maintainer  : joaopedro.pianta@gmail.com
-}

module Main (main) where

import qualified Data.Text.IO        as T
import           System.Environment
import           System.Console.GetOpt

import           GitLabCi.Docker
import           GitLabCi.Shell

-- |Options received as flags: Help, Input file and Output file
data Options = Options  { optHelp       :: Bool
                        , optInput      :: String
                        , optOutput     :: String
                        } deriving (Show)

-- |Default flag values
defaultOptions :: Options
defaultOptions = Options  { optHelp    = False
                        , optInput      = ""
                        , optOutput     = "Dockerfile"
                        }

-- |Header output, for help
header :: String
header = "Usage: gitlab-ci [OPTIONS...] inputFile..."

-- |Base String to show help options
showOptions :: String
showOptions = usageInfo header options

-- |Options descriptions and how they modify the Options object
options :: [ OptDescr (Options -> Options) ]
options =
    [ Option "i" ["input"]
        (ReqArg (\i opts -> opts { optInput = i }) "FILE")
        "Input file"

    , Option "o" ["output"]
        (ReqArg (\o opts -> opts { optOutput = o }) "FILE")
        "Output file"

    , Option "h" ["help"]
        (NoArg (\opt -> opt { optHelp = True }))
        "Show Help"
    ]

-- |Compile args to verify if there was an error or an unidentified flag received
compileOpts :: [String] -> IO (Options, [String])
compileOpts argv =
  case getOpt Permute options argv of
      (o,n,[]  ) -> return (foldl (flip id) defaultOptions o, n)
      (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))

-- |Execute commands based of the options received.\n 
-- |Show help if -h was received;\n
-- |Show error if an input file name was not received, neither by args or -i flag;\n
-- |Otherwise, execute writeDockerFileFromGitLabCI. -i flag has preference over args for input file name.
executeCommands :: (Options, [String]) -> IO ()
executeCommands (Options{optHelp=True}, _)                   = putStrLn showOptions
executeCommands (Options{optInput=""}, [])                   = ioError (userError ("No input file specified\n" ++ showOptions))
executeCommands (Options{optInput="", optOutput=out}, [inp]) = process inp out
executeCommands (Options{optInput=inp, optOutput=out}, _)    = process inp out

main :: IO ()
main = getArgs >>= compileOpts >>= executeCommands

process inp out = do
  parsed <- readGitLabCiFile inp
  writeGitlabDockerFiles out parsed
  T.writeFile (out <> ".sh") $ gitLabCiScript out parsed
