---
title: GitLabCI emulation in Docker
author: Michał J. Gajda
date: Jan 24 2021
---

# TL;DR

Simulate GitLabCI runs with Docker:

1. Parse GitLabCI with YAML
2. Generate Dockerfile for every job
   - Generation only: https://hackage.haskell.org/package/dockerfile
   - Parser: https://hackage.haskell.org/package/language-docker
   - Generator: https://github.com/hadolint/dockerfile-creator
3. Generate shell script to run them all.

It should consist of two components:

A) Library for parsing `.gitlab-ci.yml` files.
B) Executable for generating Dockerfiles and shell scripts to run them.

# Schedule

1. First parse three example `.yaml` files,
and generate corresponding Dockerfile for each job.

Docker image to start with is given by `image:` key.

2. Support variables and running different stages in order.
  - this will require generating `.sh` that runs them in order and copies the data

3. Support for all necessary entries.

4. Make it available as open source on company site.
   Document it.

# References

[1] GitLab CI YAML file documentation: https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html
[2] GitLab CI documentation: https://docs.gitlab.com/ee/ci/
[3] 
