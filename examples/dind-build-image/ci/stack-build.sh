#!/bin/bash

source ci/common.sh

rm -f stack.yaml.lock # Temporary measure during Stack upgrade

STACK_OPTS="--test --bench --haddock --fast"

message "Build"
stack build   ${STACK_OPTS}

message "Package tarballs"
mkdir -p sdist
stack sdist   --test --tar-dir=sdist

message "Check coverage for curl-parse"
stack run -- curl-parse -i inputs |grep csv|grep '%'

message "Check API generation for servant-gen"
(cd servant-gen;
 source scripts/local-env.sh;
 stack run generator -- --signature "${CI_COMMIT_SLUG}" --api mailgun;
 stack run generator -- --signature "${CI_COMMIT_SLUG}" --api cloudflare;
 cd dist/mailgun;
 stack test || error "Generated API fails to compile"
)
