#!/bin/bash

source ci/common.sh

message "Versions"
cabal --version
ghc   --version
hpack --version

message "Hpack"
ls toolkit/*
for i in toolkit/*; do
  (cd $i; hpack)
done
ls toolkit/*/*.cabal

message "Dependencies"
cabal update
cabal install --dependencies-only --enable-tests

message "Build"
cabal v1-configure --enable-tests --allow-newer
cabal v1-build
cabal v1-test

message "Prepare release artifacts"
mkdir -p bin sdist
cabal install --bindir=bin/
cabal sdist   --builddir=sdist/
cabal haddock --builddir hackage-docs --for-hackage

