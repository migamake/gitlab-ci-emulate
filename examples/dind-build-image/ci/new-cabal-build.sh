#!/bin/bash

source ci/common.sh

message "Versions"
cabal --version
ghc   --version
hpack --version

message "Hpack"
for i in deps/*; do
  (cd $i; hpack)
done

message "Dependencies"
cabal update

message "Build"
cabal build --enable-tests --allow-newer
cabal test --allow-newer

message "Prepare artifacts"
mkdir -p bin sdist
cabal install --bindir=bin/ --allow-newer
cabal sdist   --builddir=sdist/
cabal haddock --builddir hackage-docs --haddock-for-hackage

