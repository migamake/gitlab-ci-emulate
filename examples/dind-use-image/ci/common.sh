#!/bin/bash

message () {
  echo -e "\e[1m\e[33m${*}\e[0m"
}
error () {
  echo -e "\e[1m\e[31m${*}\e[0m"
}
set -o verbose
set -o xtrace
set -o pipefail
set -o errexit
set -o nounset

SCRAPING_JOBS="cloudflare mailgun upwork gitlab office365 transferwise gitlab webdriver dropbox site24x7 azure digitalocean backblaze linode ibmcloud vultr"
