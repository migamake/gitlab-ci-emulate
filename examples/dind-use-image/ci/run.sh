#!/bin/bash

source ci/common.sh;

echo "Update parameters syntax example JSON."
yaml2json parameters-syntax-example.yaml |json_pp > parameters-syntax-example.json || error "ERROR CANNOT PRETTY EXAMPLE"

mkdir -p output

echo "Scraping jobs to run ${SCRAPING_JOBS}.";
for JOB in ${SCRAPING_JOBS}; do
message "Scraping: ${JOB}";
(cd "${JOB}";
 rm -f "${JOB}.csv";
 bash "${JOB}.sh" </dev/null || error "ERROR RUNNING SCRAPER ${JOB}";
 echo "COPYING ${JOB}.csv";
 cp "${JOB}.csv" ../output/ || error "ERROR MISSING OUTPUT OF ${JOB}") || echo "FAILED: ${JOB}" ;
done;

