#!/bin/bash

echo "Install Ubuntu packages"
export DEBIAN_FRONTEND=noninteractive
apt-get update
timeout 10m apt-get install -y python3-scrapy python3-pip perl netbase ca-certificates chromium-chromedriver chromium-browser locales apt-utils libghc-yaml-dev
#For 20.04: python-is-python3
apt-get clean \

echo "Install Python packages"
pip3 install -r requirements.txt
rm -rf /var/lib/apt/lists

