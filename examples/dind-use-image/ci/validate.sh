#!/bin/sh
# Only /bin/sh shell is available in Docker image

source ci/common.sh
#SCRAPING_JOBS="cloudflare mailgun upwork gitlab office365 transferwise gitlab webdriver dropbox site24x7"

echo "Container registry login"
docker login -u "${PARSER_CONTAINER_USER}" -p "${PARSER_CONTAINER_TOKEN}" "${PARSER_CONTAINER_REGISTRY}"

echo "Pull validation container"
docker pull "${PARSER_CONTAINER_REGISTRY}:master"

echo "Run validation on CSV files"
docker run -v "${PWD}/output:/workdir" "${PARSER_CONTAINER_REGISTRY}:master" \
  $(for JOB in ${SCRAPING_JOBS}; do echo -i ${JOB}.csv; done);

echo "Clean credential"
rm -f /root/.docker/config.json
