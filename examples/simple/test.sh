#!/bin/bash
# Check unit tests
stack test &&
# Check that examples run without crashing
stack run -- -f examples/example.csv -s output1              &&
stack run -- -f examples/example_multiple_patterns.csv --all &&
stack run -- -f examples/non-utf8.csv                        &&
stack run -- -f examples/example_multiple_patterns_cutoff.csv
